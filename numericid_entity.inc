<?php
/**
 * @file
 * Classes to use as entity controller classes.
 */

/**
 * @class
 * Default numeric id entity controller class.
 */
class DrupalNumericEntityController extends DrupalDefaultEntityController {

  /**
   * Validate numeric ids.
   */
  static public function validateNumeric($ids = array()) {
    // Clean the $ids array to remove non-integer values that can be passed
    // in from various sources, including menu callbacks.
    if (is_array($ids)) {
      foreach ($ids as $key => $id) {
        $id = (string) $id;
        if ((empty($id) && $id !== '0') || ($id !== (string) (int) $id)) {
          unset($ids[$key]);
        }
      }
    }
    return $ids;
  }

  /**
   * Implements DrupalEntityControllerInterface::load().
   */
  public function load($ids = array(), $conditions = array()) {
    $ids = self::validateNumeric($ids);
    return parent::load($ids, $conditions);
  }

}

class DrupalNumericNodeController extends NodeController {

  /**
   * Implements DrupalEntityControllerInterface::load().
   */
  public function load($ids = array(), $conditions = array()) {
    $ids = DrupalNumericEntityController::validateNumeric($ids);
    return parent::load($ids, $conditions);
  }
}

class DrupalNumericUserController extends UserController {

  /**
   * Implements DrupalEntityControllerInterface::load().
   */
  public function load($ids = array(), $conditions = array()) {
    $ids = DrupalNumericEntityController::validateNumeric($ids);
    return parent::load($ids, $conditions);
  }
}

if (module_exists('entitycache')) {
  /**
   * Node entity controller with persistent cache.
   */
  class EntityCacheNumericNodeController extends EntityCacheNodeController {

    /**
     * Implements DrupalEntityControllerInterface::load().
     */
    public function load($ids = array(), $conditions = array()) {
      $ids = DrupalNumericEntityController::validateNumeric($ids);
      return parent::load($ids, $conditions);
    }
  }
}
